#!/bin/bash


echo "Enter username:"
read username
echo "Enter password:"
read -s password

    if [[ "$password" == "$username" ]]; then
        echo "Password cannot be the same as username"
        exit 1
    fi
    
    if [[ "$password" =~ "chicken" || "$password" =~ "ernie" ]]; then
        echo "Password cannot contain the words chicken or ernie"
        exit 1
    fi
    
    if [[ ${#password} -lt 8 ]]; then
        echo "Password must be at least 8 characters long"
        exit 1
    fi

    if ! [[ "$password" =~ [A-Z] && "$password" =~ [a-z] ]]; then
        echo "Password must have at least one uppercase and one lowercase letter"
        exit 1
    fi

    if ! [[ "$password" =~ [0-9] && "$password" =~ [A-Za-z] ]]; then
        echo "Password must be alphanumeric"
        exit 1
    fi
    
# failed code
# (failed) dir="/home/slncr/sysop"

#if [ ! -f "$dir/users.txt" ]; then
#   touch "$dir/users.txt"
#    echo "File created."
mkdir /home/slncr/sysop/users.txt
if grep -q "^$username$" /home/slncr/sysop/users.txt; then
    echo "User already exists"
    echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: ERROR User already exists" >> log.txt
    exit 1
fi

echo "$username" >> /home/slncr/sysop/users.txt
echo "User registered successfully"
echo "$(date '+%y/%m/%d %H:%M:%S') REGISTER: INFO User $username registered successfully" >> log.txt
exit 0


