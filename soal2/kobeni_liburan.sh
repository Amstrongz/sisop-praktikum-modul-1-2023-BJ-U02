#!/bin/bash

time=$(date +"%H")

if [ $time == "00" ]; then
  quantity=1
else
  quantity=$time
fi

start=$(($time / 10 * 10))

folder_name="kumpulan_$(( $(ls -d kumpulan_* | wc -l) + 1 ))"
mkdir "$folder_name"

for (( i=1; i<=$quantity; i++ ))
do
  file_name="perjalanan_$i.jpg"
  wget "https://source.unsplash.com/1600x900/?indonesia" -O "$folder_name/$file_name"
done

0 */10 * * * bash /path/to/twowhole.sh
0 0 * * * cd /path/to/home/slncr/sysop && zip -r "devil_$(ls -d devil_* | wc -l).zip" *
